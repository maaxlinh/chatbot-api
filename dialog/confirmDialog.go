package dialog

import (
	"github.com/michlabs/fbbot"
)

type OptionMess struct {
	name string
	imagelink string
}

type ConfirmDialog struct {
	fbbot.BaseStep
	QuickItems []OptionMess
}

func CreateSelectOption(bot *fbbot.Bot, msg *fbbot.Message,mess string, options []OptionMess){
	var items  []fbbot.QuickRepliesItem

	for _, option := range options {
		items = append(items, fbbot.QuickRepliesItem{
			ContentType: "Text",
			Title:       option.name,
			Payload:     option.name,
			// ImageURL:    T("select_human_icon"),
		})
	}
	botSelect := new(fbbot.QuickRepliesMessage)
	botSelect.Items = items
	botSelect.Text = mess
	bot.Send(msg.Sender, botSelect)
}

type ImageUploadConfirmDialog struct {
	ConfirmDialog
}

func (s ImageUploadConfirmDialog) Enter(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {

	s.QuickItems = append(s.QuickItems, OptionMess{"Finish",""})
	s.QuickItems = append(s.QuickItems, OptionMess{"Upload more",""})
	CreateSelectOption(bot,msg,"are you finish?",s.QuickItems)
	return ""
}

func (s ImageUploadConfirmDialog) Process(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {
	if msg.Text=="Finish" {
		return "finish upload dialog"
	}else if msg.Text=="Upload more"{
		return "image dialog"
	}
	return "image dialog"
}