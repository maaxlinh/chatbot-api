package dialog

import (
	"github.com/michlabs/fbbot"
	"os"
	"errors"
	"strconv"
	"time"
	"math/rand"
	"path/filepath"
	"bitbucket.org/maaxlinh/chatbot-api/Util"
	"encoding/json"
)

type BVimage struct {
	*fbbot.Image
	FBUserID string `json:"fbserid"`
	DocID    string `json:"docid"`
	LocalURL string `json:"localurl"`
}

func (s BVimage) GenLocalPath(mainfolder string) string {
	rand_name := strconv.FormatInt(time.Now().Unix(), 10) + "_" + strconv.Itoa(rand.Intn(1000))

	localimagepath := filepath.Join(mainfolder,
		s.FBUserID,
		s.DocID+rand_name+".jpg",
	)
	return localimagepath
}

type ImageDialog struct {
	fbbot.BaseStep
}

const matl string = "12345"

func (s ImageDialog) Enter(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {
	bot.STMemory.For(msg.Sender.ID).Set("matl", matl)
	bot.SendText(msg.Sender, "plz take a picture!")
	return ""
}

func (s ImageDialog) Process(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {
	bot.SendText(msg.Sender, "processing the picture!")
	//check images number
	if len(msg.Images) < 1 {
		bot.SendText(msg.Sender, "i have not receaved any picture!")
		return fbbot.Event("error")
	}

	//save images to local
	bot.SendText(msg.Sender, "save images to local!")
	images, e := saveImageTolocal(bot, msg, os.Getenv("IMAGES_FOLDER"))
	if e != nil {
		return fbbot.Event("error")
	}

	//save info to short time memory
	bot.SendText(msg.Sender, "save info to short time memory!")
	e = saveInfo(images, bot, msg)
	if e != nil {
		return fbbot.Event("error")
	}

	return fbbot.Event("image confirm dialog")
}

func saveInfo(images []BVimage, bot *fbbot.Bot, msg *fbbot.Message) (error) {
	matlValue := bot.STMemory.For(msg.Sender.ID).Get("matl")

	stMemoryKey := msg.Sender.ID + "_" + matlValue
	stMemoryValue := bot.STMemory.For(msg.Sender.ID).Get(stMemoryKey)
	if stMemoryValue != "" {
		//bot.Logger.Infof("stMemoryValue :", stMemoryValue)
		var memoryImage []BVimage
		err := json.Unmarshal([]byte(stMemoryValue),&memoryImage)
		if err != nil {
			bot.Logger.Errorf(" unmarshal json value fail!")
			return errors.New("cant unmarshal json value!")
		}
		for i := 0; i < len(memoryImage); i++ {
			images = append(images, memoryImage[i])
		}
		//bot.Logger.Infof("marshal OK memoryImage leng :", len(images))
	}
	jsonvar, _ := json.Marshal(images)
	bot.Logger.Infof("json data",string(jsonvar))
	bot.STMemory.For(msg.Sender.ID).Set(msg.Sender.ID+"_"+matlValue, string(jsonvar))
	return nil
}

func saveImageTolocal(bot *fbbot.Bot, msg *fbbot.Message, location string) ([]BVimage, error) {
	if location == "" {
		return nil, errors.New("location is null!")
	}
	var BVimages []BVimage
	for i := 0; i < len(msg.Images); i++ {
		//localimagepath := location + msg.Sender.ID + "_" + strconv.Itoa(i) + ".jpg"
		newImage := BVimage{&msg.Images[i],
							msg.Sender.ID,
							bot.STMemory.For(msg.Sender.ID).Get("matl"),
							""}
		newImage.LocalURL = newImage.GenLocalPath(location)
		err :=Util.SaveImage(newImage.LocalURL, newImage.URL)
		if err != nil {
			return nil,	err
		}
		BVimages = append(BVimages, newImage)
	}

	return BVimages, nil
}
