package dialog

import "github.com/michlabs/fbbot"

type FinishUploadDialog struct {
	fbbot.BaseStep
}

func (s FinishUploadDialog) Enter(bot *fbbot.Bot, msg *fbbot.Message) fbbot.Event {

	return "next dialog"
}
