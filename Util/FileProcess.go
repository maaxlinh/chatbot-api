package Util

import (
	"net/http"
	"log"
	"os"
	"io"
	"github.com/pkg/errors"
	"path/filepath"
)

func SaveImage(savePath string,URL string)(error){
	response, e := http.Get(URL)
	if e != nil {
		log.Fatal(e)
		return e
	}
	defer response.Body.Close()

	//if file path is nil
	if savePath == ""{
		return errors.New("savePath s Null")
	}
	//check if exist
	folder := filepath.Dir(savePath)
	if _, err := os.Stat(folder); os.IsNotExist(err) {

		os.MkdirAll(folder,os.ModePerm)
	}

	//open a file for writing
	file, err := os.Create(savePath)
	if err != nil {
		log.Fatal(err)
	}
	// Use io.Copy to just dump the response body to the file. This supports huge files
	_, err = io.Copy(file, response.Body)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	return  nil
}


